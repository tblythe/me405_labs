## @file mainpage.py
#  @author Trevor Blythe
#  @mainpage
#
#  @section sec_intro Introduction
#  Lab 1 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_1/  
#
#  @section sec_motor Motor Driver
#  Motor driver lab that runs motor based on duty cycle.
#
#  @section sec_enc Encoder Lab 2
#  Lab 2 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_2/
#
#  @section sec_lin Lab 3
#  Lab 3 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_3/
#
#  @section sec_imc Lab 4
#  Lab 4 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_4/
#
#
#  @page page_imu Intertial measurement unit page (Lab 4)
#
#  To test the calibration, I drew a series of lines, first at 90 degree angles, and
#  then I split those angles in two. This created a basic compass, allowing me
#  to test the output of the IMU as I turned it angle by angle.
#
#  Video : https://bitbucket.org/tblythe/me405_labs/src/master/Lab_4/VIDEO0084.mp4
#
#  Repository : https://bitbucket.org/tblythe/me405_labs/src/master/Lab_4/
#
#  @page page_TermProject ME 405 Term Project: "Do Nothing Box"
#
#  @section sec_scope Project Scope and Completion
#  The do-nothing box was a project designed and coded by Jose Chavez,
#  Matthew Tagupa, and myself. In addition, we each built our own box.
#  The code was designed in such a way as to allow a certain amount of
#  flexibility between box designs, since each box would function slightly
#  differently based on materials used and design details.
#
#  I was not able to purchase as many parts as Jose and Matt, so I made
#  due mostly with parts I had around the house. This lead to some
#  design differences between theirs as well as the original plan.
#
#  The main difference was the replacement of a door or lid (to be opened
#  with the motor), with a small character from a Pez dispenser. This was done
#  first off, because I wanted a character of some kind from the beginning, but
#  found it too complicated to design a box with a lid as well as a character. It
#  also allowed for me to build a setup that was not as likely to burn out the motor,
#  since the door can be relatively heavy.
#
#  @section sec_box Physical build
#  The build first centered around finding a stable way to house the motors and switch. This is
#  because these are the moving parts, and also because they need to stay within sync with
#  each other, otherwise the pieces may collide. This was achieved using a section of 2x2 wood,
#  and homemade motor brackets, fashioned from strips of tin from a tin can. The strips of tin
#  were cut using tin snips, and shaped using a small pair of tin clamps. 
#
#  @image html 1.jpg
#
#  Once the motors were mounted, an action figure leg and a pez dispenser rabbit
#  were added, to give the box some character. The switch was modified and then mounted.
#  The first modification was to separate the upper and lower housing, so that there was
#  a small gap, in order to soften the throw. In additiong, a popsicle stick was affixed
#  to the lever, in order to provide greater leverage for the leg as it switches it off.
#  Both of these modificiation were done because testing showed that the switch throw
#  was entirely too stiff for the lab as designed.
#
#  @image html 2.jpg
#
#  Finally, the entire setup was placed in a leftover box. The box was not overly important,
#  however, if I decide to work on this project after class is out, a first step would
#  be to find a more sturdy box.
#
#  @image html 3.jpg
#
#  A video can be seen here :
#  https://
#
#  @section sec_coding Coding
#  Coding for the lab was relatively simple, with one of the largest changes
#  from previous labs being the addition of the ToggleSwitch class. This class
#  was created in order to use a switch as a control input for the nucleo. 
#
#
#  @section sec_conclusion Conclusion and moving forward
#  Overall, the project was a success, and I was especially happy with the fit and function
#  of the motor clamps and housing, given how I had to use whatever I had laying around.
#  I was not as happy with the box, but it did fit with our initial plans.
#
#  Moving forward, there are improvements that can be implemented. Other than the box as previously
#  mentioned, I would want to use a more polished creature, instead of two random
#  pieces. Also, it could have more "character" by implementing certain movements, for example,
#  by moving more erratically, as if angry. In addition, subsequent switch throws
#  could lead to more and more erratic movement, as if the creature is getting more upset. A further
#  addition to this could use a certain amount of randomness, so that the movements themselves
#  are not always exactly the same, and don't always follow in the same order.

#
#  @author Trevor Blythe
#
#  @copyright License Info
#
#  @date June 12, 2020