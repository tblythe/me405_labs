## @file DoNothingBox.py
#  This is the main code for the do nothing box. The setpoints,
#  Theta_ref1 and Theta_ref2, should be adjusted as needed for each box.
#
#  @author Trevor Blythe, Jose Chavez, Matthew Tagupa
#
#  @date June 12, 2020
#
#  @package DoNothingBox
#
#  The main code for the DNB, it opens the hatch (or raises the character).
#  When the switch is flipped by the arm, the arm goes back inside the
#  box, and the door closes (or the character disappears). 
#
import pyb
import utime

from MotorDriver import MotorDriver

from Encoder import Encoder

from PropControl import CLPropControl

from ToggleSwitch import ToggleSwitch
    
if __name__ == '__main__':
    
## Initialization-------------------------------------------------------------
        
    #  Motor Driver 1 (A)-----------------------------------------------------
    #
    #  Create the pin objects used for interfacing with the motor driver
    pin_EN_A  = pyb.Pin.cpu.A10
    pin_IN1_A = pyb.Pin.cpu.B4
    pin_IN2_A = pyb.Pin.cpu.B5

    ## Create the timer object used for PWM generation (3 or 5)
    timer_number_A = 3

    ## Designate the frequency
    freq_A = 20000

    ## Create a motor object passing in the pins and timer
    moeA = MotorDriver(pin_EN_A, pin_IN1_A, pin_IN2_A, timer_number_A, freq_A)

    #  Enable the motor driver
    moeA.enable()

    #  Set the duty cycle to 0 percent to start. It will be set by the 
    #  controller
    moeA.set_duty(0)
    
    #  Motor Driver 2 (B)-----------------------------------------------------
    
    #  Create the pin objects used for interfacing with the motor driver
    pin_EN_B  = pyb.Pin.cpu.C1
    pin_IN1_B = pyb.Pin.cpu.A0
    pin_IN2_B = pyb.Pin.cpu.A1

    ## Create the timer object used for PWM generation (3 or 5)
    timer_number_B = 5

    ## Designate the frequency
    freq_B = 20000

    ## Create a motor object passing in the pins and timer
    moeB = MotorDriver(pin_EN_B, pin_IN1_B, pin_IN2_B, timer_number_B, freq_B)

    #  Enable the motor driver
    moeB.enable()

    #  Set the duty cycle to 0 percent to start. It will be set by the 
    #  controller
    moeB.set_duty(0)
    
    #  Encoder 1--------------------------------------------------------------
    #
    #  Creates the encoder attached to Motor 1
    
    # Designate encoder timer number (4 or 8)
    Timer1 = 4
    
    # Designate prescaler
    prescaler = 0
    
    # Designate period
    period = 65535
    
    # Designate pins for Encoder A and Encoder B on the motor
    EncoderA1 = pyb.Pin.cpu.B6
    EncoderB1 = pyb.Pin.cpu.B7
    
    # Enable the Encoder
    enco1 = Encoder(Timer1, EncoderA1, EncoderB1, prescaler, period)
    
    #  Need to initialize this current delta for the actuation value
    #  calculations
    delta_sum1 = 0
    
    #  Encoder 2--------------------------------------------------------------
    #
    #
    
    # Designate encoder timer number (4 or 8)
    Timer2 = 8
    
    # Designate prescaler
    prescaler = 0
    
    # Designate period
    period = 65535
    
    # Designate pins for Encoder A and Encoder B on the motor
    EncoderA2 = pyb.Pin.cpu.C6
    EncoderB2 = pyb.Pin.cpu.C7
    
    # Enable the Encoder
    enco2 = Encoder(Timer2, EncoderA2, EncoderB2, prescaler, period)
    
    #  Need to initialize this current delta for the actuation value
    #  calculations
    delta_sum2 = 0


    ## PropControl 1----------------------------------------------------------
    #
    #  We need to pass in the Kp value to initialize the proportional 
    #  controller. This is also where we are going to define the setpoint.
    Kp1 = 0.1
    PropControl1 = CLPropControl(Kp1)
    
    ## PropControl 2----------------------------------------------------------
    #
    #  We need to pass in the Kp value to initialize the proportional 
    #  controller. This is also where we are going to define the setpoint.
    Kp2 = 0.15
    PropControl2 = CLPropControl(Kp2)

    ## Toggle Switch----------------------------------------------------------
    #
    # Create input pin Location
    Switch_Pin = pyb.Pin.cpu.A7
    Toggle = ToggleSwitch(Switch_Pin)
    
## Main Program---------------------------------------------------------------

    #  Loop to have the program constantly running
    while(True):
        
        # Value of the Switch
        Switch_State = Switch_Pin.value()
        
        #  Condition for switch in the ON position
        if (Switch_State == 1):
            
            #  Opening the Door
            for i in range (0, 100):
                
                #  We first need to call on the encoder update method to get 
                #  the current position and the difference in that position 
                #  from the previous position of the motor.
                enco1.update() 
                delta_update1 = enco1.delta

                #  We are now going to add the accumulated positions together 
                #  to know how much closer the motor is to the desired 
                #  setpoint
                delta_sum1 = delta_sum1 + delta_update1
                
                #  This next line of code is not necessary, I included it with 
                #  the intent to help me and the user understand what is being 
                #  passed back to the closed-loop proportional controller.
                Theta_Measured1 = delta_sum1
            
                #  Call the update method from the proportional controller so 
                #  that the actuation value can be given to the MotorDriver 
                #  class to change the value of the PWM
                Theta_ref1 = 350
                
                PropControl1.update(Theta_ref1, Theta_Measured1)
                actuation1 = PropControl1.actuation_value
            
                #  Update the MotorDriver duty cycle (PWM)
                moeA.set_duty(actuation1)

                #  This is a delay that will allow the recording of the values 
                #  to be more gradual not performing this may cause the 
                #  program to crash due to the large amount of iterations it 
                #  must perform. 
                utime.sleep_ms(10)

            else:
                #  When the for loop is done, this will disable the motor and 
                #  proceed
                moeA.set_duty(0)
                
            #  Moving the Lever Arm UP
            for i in range (0, 200):
                
                #  We first need to call on the encoder update method to get 
                #  the current position and the difference in that position 
                #  from the previous position of the motor.
                enco2.update() 
                delta_update2 = enco2.delta

                #  We are now going to add the accumulated positions together 
                #  to know how much closer the motor is to the desired 
                #  setpoint
                delta_sum2 = delta_sum2 + delta_update2
                
                #  This next line of code is not necessary, I included it with 
                #  the intent to help me and the user understand what is being 
                #  passed back to the closed-loop proportional controller.
                Theta_Measured2 = delta_sum2
            
                #  Call the update method from the proportional controller so 
                #  that the actuation value can be given to the MotorDriver 
                #  class to change the value of the PWM
                Theta_ref2 = 2050
                
                PropControl2.update(Theta_ref2, Theta_Measured2)
                actuation2 = PropControl2.actuation_value
            
                #  Update the MotorDriver duty cycle (PWM)
                moeB.set_duty(actuation2)

                #  This is a delay that will allow the recording of the values 
                #  to be more gradual not performing this may cause the 
                #  program to crash due to the large amount of iterations it 
                #  must perform. 
                utime.sleep_ms(10)

            else:
                #  When the for loop is done, this will disable the motor and 
                #  proceed
                moeB.set_duty(0)
                
            #  Moving the Lever Arm DOWN
            for i in range (0, 100):
                
                #  We first need to call on the encoder update method to get 
                #  the current position and the difference in that position 
                #  from the previous position of the motor.
                enco2.update() 
                delta_update2 = enco2.delta

                #  We are now going to add the accumulated positions together 
                #  to know how much closer the motor is to the desired 
                #  setpoint
                delta_sum2 = delta_sum2 + delta_update2
                
                #  This next line of code is not necessary, I included it with 
                #  the intent to help me and the user understand what is being 
                #  passed back to the closed-loop proportional controller.
                Theta_Measured2 = delta_sum2
            
                #  Call the update method from the proportional controller so 
                #  that the actuation value can be given to the MotorDriver 
                #  class to change the value of the PWM
                Theta_ref2 = 75
                
                PropControl2.update(Theta_ref2, Theta_Measured2)
                actuation2 = PropControl2.actuation_value
            
                #  Update the MotorDriver duty cycle (PWM)
                moeB.set_duty(actuation2)

                #  This is a delay that will allow the recording of the values 
                #  to be more gradual not performing this may cause the 
                #  program to crash due to the large amount of iterations it 
                #  must perform. 
                utime.sleep_ms(10)

            else:
                #  When the for loop is done, this will disable the motor and 
                #  proceed
                moeB.set_duty(0)
                
            #  Closing the Door
            for i in range (0, 100):
                
                #  We first need to call on the encoder update method to get 
                #  the current position and the difference in that position 
                #  from the previous position of the motor.
                enco1.update() 
                delta_update1 = enco1.delta

                #  We are now going to add the accumulated positions together 
                #  to know how much closer the motor is to the desired 
                #  setpoint
                delta_sum1 = delta_sum1 + delta_update1
                
                #  This next line of code is not necessary, I included it with 
                #  the intent to help me and the user understand what is being 
                #  passed back to the closed-loop proportional controller.
                Theta_Measured1 = delta_sum1
            
                #  Call the update method from the proportional controller so 
                #  that the actuation value can be given to the MotorDriver 
                #  class to change the value of the PWM
                Theta_ref1 = 0
                
                PropControl1.update(Theta_ref1, Theta_Measured1)
                actuation1 = PropControl1.actuation_value
            
                #  Update the MotorDriver duty cycle (PWM)
                moeA.set_duty(actuation1)

                #  This is a delay that will allow the recording of the values 
                #  to be more gradual not performing this may cause the 
                #  program to crash due to the large amount of iterations it 
                #  must perform. 
                utime.sleep_ms(10)

            else:
                #  When the for loop is done, this will disable the motor and 
                #  proceed
                moeA.set_duty(0)
        
        #  Condition for switch in the OFF position
        else:
            continue