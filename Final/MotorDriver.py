## @file MotorDriver.py
#  The motor driver was built connecting a X-NUCLEO-IHM04A1 motor driver 
#  platform and a NUCLEO-L476RG. The motor was connected to the A ports in 
#  the IHM04A1 along with connecting a 12V power source to port B. The program 
#  was coded in python and uses Thonny to interact between the code and the 
#  microcontroller.
#
#  The user will be responsible for designating and defining the pins (EN, 
#  IN1, and IN2), designating the timer channel, and designating a frequency. 
#  The example in the main portion of this code gives an example of how to 
#  implement these designations. After connecting the motors to the correct
#  ports and defining the module with the "MotorDriver" class, the user can 
#  then activate or deactivate the motor by using the enable and disable
#  methods as well as set a pulse width module (PWM) value for the motor.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020
#
#  @package MotorDriver
#  The motor driver was built connecting a X-NUCLEO-IHM04A1 motor driver 
#  platform and a NUCLEO-L476RG. The motor was connected to the A ports in 
#  the IHM04A1 along with connecting a 12V power source to port B. The program 
#  was coded in python and uses Thonny to interact between the code and the 
#  microcontroller.
#
#  MotorDriver.py consists of three methods: enabling, disabling, and 
#  changing the duty cycle of the motor. Each can be called with the commands 
#  ".enable", ".disable", and ".set_duty(duty)", respectively. The user will 
#  be responsible for designating and defining the pins (EN, IN1, and IN2), 
#  designating the timer channel, and designating a frequency. The example in 
#  the main portion of this code gives an example of how to implement these 
#  designations. After connecting the motors to the correctports and defining 
#  the module with the "MotorDriver" class, the user can then activate or 
#  deactivate the motor by using the enable and disablemethods as well as set 
#  a pulse width module (PWM) value for the motor.
#
#  @author Matthew Tagupa
#
#  @copyright License Info
#
#  @date May 21, 2020

import pyb

## A motor driver object
#
#  This class implements the motor driver for our ME 405 board.
#
#  @author Matthew Tagupa
#  @copyright License Info
#  @date April 29, 2020
class MotorDriver:

    ## Constructor for motor driver
    #
    #  Creates a motor driver by initializing GPIO pins and turning the motor 
    #  off for safety.
    #  @param pinEN A pyb.Pin object to use as the enable pin.
    #  @param pinIN1 A pyb.Pin object to use as the input to half bridge 1.
    #  @param pinIN2 A pyb.Pin object to use as the input to half bridge 2.
    #  @param timer_number A pyb.Timer object to use for PWM generation on pinIN1_A
    #  and pinIN2_A.
    #  @param frequency A timer characteristic 
    def __init__ (self, pinEN, pinIN1, pinIN2, timer_number, frequency):
        print ('Creating a motor driver')
        
        #  These will store the variables input as class attributes
        self.pinEN = pinEN
        self.pinIN1 = pinIN1
        self.pinIN2 = pinIN2
        self.timer_number = timer_number
        
        #  Initialize the timer
        timer = pyb.Timer(self.timer_number, freq = frequency)
        
        #  Initialize all of the pins
        self.pinEN  = pyb.Pin(self.pinEN, pyb.Pin.OUT_PP)
        self.timch1 = timer.channel(1, pyb.Timer.PWM, pin=self.pinIN1)
        self.timch2 = timer.channel(2, pyb.Timer.PWM, pin=self.pinIN2)
        
        #  Disable the motor at the beginning for safety measures
        self.pinEN.low()
        
        #  This is used as a toggle between the motor driver being enabled and
        #  disabled.
        self.pinEN_toggle = 0   

    ## Enable the Motor
    #
    #  This method enables the motor. If the motor is already enabled, it will
    #  warn the user that it is already enabled. 
    def enable (self):
        
        #  We must import pyb each time because the REPL will not recognize it
        #  once it is done with the function in MotorDriver 
        
        if (self.pinEN_toggle == 0):
            print ('Enabling Motor')       
            self.pinEN.high()
            
            self.pinEN_toggle = 1

            pass
        else:
            print ('Motor Already Enabled')
            pass

    ## Disables the Motor
    #
    #  This method disables the motor. If the motor is already disabled, it will
    #  warn the user that it is already disabled. 
    def disable (self):
        
        if (self.pinEN_toggle == 1):
            print ('Disabling Motor')
            self.pinEN.low()
            
            self.pinEN_toggle = 0
            pass
        else:
            print ('Motor Already Disabled')           
            pass

    ## Sets duty cycle for motor
    #
    #  This method sets the duty cycle to be sent to the motor to the given 
    #  level. Positive values cause effort in one direction, negative values 
    #  in the opposite direction.
    #
    #  @param duty A signed integer holding the duty cycle of the PWM signal 
    #  sent to the motor.
    def set_duty (self, duty):
        
        self.duty = duty
        
        #  Check to make sure that the motor is enabled
        if (self.pinEN_toggle == 0):
            print('Motor is currently disabled. Enable the motor to make it spin.')
        else:
            pass
        
        
        if (self.duty > 0):
            #print('Spinning the motor CCW')
            
            #  According to the L6206 datasheet, IN2 or IN1 must be set low 
            #  and the EN pin must be set high to get the motor to run. Since 
            #  the motor will have already been enabled by the enable method, 
            #  no need ot enable it here. After setting the duty cycle for IN1 
            #  and testing, this spins the motor counter-clockwise.
            self.timch1.pulse_width_percent(self.duty) 
            self.timch2.pulse_width_percent(0)
            
        elif (self.duty < 0):                        
            #print('Spinning the motor CW')
            
            #  The motor cannot distinguish the direction it is supposed to 
            #  spin. By switching the PWM value of the IN1 and the IN2 pins,
            #  we can spin the motor in different directions. We first 
            #  need to allow the user to enter a negative number for the 
            #  motor to spin clockwise.
            self.duty = abs(self.duty)

            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(self.duty)
            
        elif (self.duty == 0):
            #  In case the user enters "0" for the duty cycle, the program 
            #  will warn the user that there is no output because there is no
            #  duty cycle.
            #print('Stopping Motor. Enter a new duty cycle.')
            
            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(0)
            
        else:
            #  If the user enters something that is not a number, this error
            #  message will prompt the user to enter an integer next time.
            print('The value of the PWM must be an integer')

        pass
    
if __name__ == '__main__':
# Adjust the following code to write a test program for your motor class. Any
# code within the if __name__ == '__main__' block will only run when the
# script is executed as a standalone program. If the script is imported as
# a module the code block will not run.

    # Motor 1
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN_A  = pyb.Pin.cpu.A10
    pin_IN1_A = pyb.Pin.cpu.B4
    pin_IN2_A = pyb.Pin.cpu.B5

    # Create the timer object used for PWM generation
    timer_number_A = 3
    
    # Designate the frequency
    freq = 20000

    # Create a motor object passing in the pins and timer
    moeA = MotorDriver(pin_EN_A, pin_IN1_A, pin_IN2_A, timer_number_A, freq)

    # Enable the motor driver
    moeA.enable()

    # Set the duty cycle to 25 percent
    moeA.set_duty(25)
    
#     #  Motor 2
#     
#     # Create the pin objects used for interfacing with the motor driver
#     pin_EN_B  = pyb.Pin.cpu.C1
#     pin_IN1_B = pyb.Pin.cpu.A0
#     pin_IN2_B = pyb.Pin.cpu.A1
# 
#     # Create the timer object used for PWM generation
#     timer_number_B = 5
#     
#     # Designate the frequency
#     freq = 20000
# 
#     # Create a motor object passing in the pins and timer
#     moeB = MotorDriver(pin_EN_B, pin_IN1_B, pin_IN2_B, timer_number_B, freq)
# 
#     # Enable the motor driver
#     moeB.enable()
# 
#     # Set the duty cycle to 25 percent
#     moeB.set_duty(25)