## @file IMU.py
#
#  @author Trevor Blythe
#
#  @date June, 2020
#
#  @package IMU
#  This package allows for use of the IMU in our kit, which has a built in
#  Accelerometer, gyroscope, and magnetometer

from pyb import I2C
from micropython import const
import utime
import ustruct
import pyb

#  All sensors active
NDOF_MODE = const(0x0C)
#  Operation mode register
OPR_MODE = const(0x3D)
#  Power mode register 
PWR_MODE = const(0x3E)
Calib_Stat = const(0x35)
#  Slave address
BNO055 = const(0x28)
GYRO_DATA = const(0x14)
EUL_DATA = const(0x1A)
 
class IMU:
  
   ## Constructor for the IMU
   #
   def __init__(self, i2c):
       self.i2c = i2c
   ## Set the IMU mode
   #
   def set_mode(self):
       i2c.mem_write(NDOF_MODE, BNO055, OPR_MODE)
   ## Get the IMU MODE
   #
   def get_mode(self):
       data = i2c.mem_read(1, BNO055, OPR_MODE)
       #  Get first value
       self.mode = ustruct.unpack('<b', data)[0]
      
       MODES = dict([('NDOF MODE', 12), ('CONFIG MODE', 0)]) 
       print('MODES :', MODES)
       if (self.mode == 12):
           print('NDOF mode')
       else:
           pass
   ## Enable the IMU
   #
   def  enable (self):
       # Set power mode to "Normal Mode"
       i2c.mem_write(PWR_MODE, BNO055, 0)
   ## This method disables the IMU
   #
   def disable (self):
       #  Set power mode to "Suspend Mode"
       i2c.mem_write(PWR_MODE, BNO055, 2)       
   ## This returns angular velocity from IMU to the uesr
   #
   def get_gyro(self):
       gyro_data = self.i2c.mem_read(8, BNO055, GYRO_DATA)
       x, y, z = ustruct.unpack('<hhh', gyro_data)
       x = int(x/16)
       y = int(y/16)
       z = int(z/16)
       return (x,y,z)
   ## This method returns the euler angle to the user
   #
   def get_eul(self):
       euler_data = self.i2c.mem_read(6, BNO055, EUL_DATA)
       x, y, z = ustruct.unpack('<hhh', euler_data)
       x = int(x/16)
       y = int(y/16)
       z = int(z/16)
       return (x,y,z)
   ## This method checks the calibration status of the IMU and returns it to the user.
   #
   def get_calibration(self):
       calibration = self.i2c.mem_read(NDOF_MODE, BNO055, Calib_Stat)
       self.magnet_cal = (calibration[0] >> 0) & 0b11
       self.gyro_cal = (calibration[0] >> 4) & 0b11
       print('Magnetometer calibration: ', self.magnet_cal,'\n'
             '\n' 'Gyroscope calibration: ', self.gyro_cal)
       
if __name__ =='__main__':
   #  Any code within the if __name__ == '__main__' block will only run when
   #  the script is executed as a standalone program. If the script is
   #  imported as a module the code block will not run.
  
   i2c = I2C(1, I2C.MASTER)
   imu = IMU(i2c)
   for i in range (0, 50):
       print('{}'.format(imu.get_eul()))
       utime.sleep_ms(1000)
