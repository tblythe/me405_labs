## @file mainpage.py
#  @author Trevor Blythe
#  @mainpage
#
#  @section sec_intro Introduction
#  Lab 1 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_1/  
#
#  @section sec_motor Motor Driver
#  Motor driver lab that runs motor based on duty cycle.
#
#  @section sec_enc Encoder Lab 2
#  Lab 2 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_2/
#
#  @section sec_lin Lab 3
#  Lab 3 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_3/
#
#  @section sec_imc Lab 4
#  Lab 4 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_4/
#
#
#  @page page_imu Intertial measurement unit (Lab 4)
#
#  To test the calibration, I drew a series of lines, first at 90 degree angles, and
#  then I split those angles in two. This created a basic compass, allowing me
#  to test the output of the IMU as I turned it angle by angle.
#
#  Video : https://bitbucket.org/tblythe/me405_labs/src/master/Lab_4/VIDEO0084.mp4
#
#  Repository : https://bitbucket.org/tblythe/me405_labs/src/master/Lab_4/
#
