''' @file main.py
There must be a docstring at the beginning of a Python
source file with an @file [filename] tag in it! '''

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''
    
    
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
            and IN2_pin. '''
        print ('Creating a motor driver')        
        import pyb
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        EN_pin = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
        EN_pin.low()
        tim3 = pyb.Timer(3, freq=5000)
        IN1_pin = pyb.Pin (pyb.Pin.cpu.B4)
        t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=IN1_pin)
        IN2_pin = pyb.Pin (pyb.Pin.cpu.B5)
        t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=IN2_pin)
        
        t3ch1.pulse_width_percent(0)
        t3ch2.pulse_width_percent(10)
        EN_pin.high()
        EN_pin.low()
        
    
    
    def enable (self):
        print ('Enabling Motor')
        EN_pin.high()

    def disable (self):
        print ('Disabling Motor')
        EN_pin.low()

    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    # Create the pin objects used for interfacing with the motor driver
    pin_EN = None;
    pin_IN1 = None;
    pin_IN2 = None;

    # Create the timer object used for PWM generation
    tim = None;

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

    # Enable the motor driver
    moe.enable()

    # Set the duty cycle to 10 percent
    moe.set_duty(10)
    

    