'''@file lab0.py

ME 405 : Lab 0
Trevor Blythe

Description : Program for calculation of fibonacci numbers. The user is 
asked to input a number, which is used as an index, and sent to function
fib(), which calculates the number based on the given index. 

'''

def fib (idx):      # Function for calculating Fibonacci number
    
    print ('')
    print ('Calculating Fibonacci number at '   # Prompt user for input   
           'index n = {:}.'.format(idx))
    if idx == 0:                        # If input is 0, print proper result
        print ('the result is 0')           
    elif idx == 1:                              
        print ('the result is 1')       # If input is 1, print proper result
    elif idx > 1:                       # Otherwise, run a loop to find n fibonacci number
        a = 0                             
        b = 1
        for i in range(1,idx):          
            fib_number = a + b          # Add two previous numbers
            a = b                       # Increment a and b as we move through each loop
            b = fib_number
        print ('the result is  ')       # After final loop, print the result
        print(fib_number) 
    else:
        print ('Improper input, index must be positive number')  # Error for non-positive number

    
if __name__ == '__main__':              # Main program for prompting user
    m_l = 0
    while m_l == 0:                     # The while loop makes the program run indefinitely             
        
        x = input('Please enter the index of the fibonacci number to be' 
                  'calculated (or e to exit): ')
        if (x.isdigit()):               # If input is a digit, proceed to function
            fib(int(x))
        else:                           # Otherwise, check for e for exit
            if x in ['e', 'E']:
                break
            else:                       # Or return error
                print('Improper input, index must be positive number')
    
    
    






