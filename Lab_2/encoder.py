'''
@file encoder.py
This is the lab 2 file, which contains code for creating
the class EncoderDriver, which reads the position of the
motor encoder for the motors uesd in ME 405
'''
import pyb

class encoder:
    ''' This class implements an encoder driver for the
    ME405 board. Syntax is Encoder(Timer number, pin 1, pin 2)
    For example : Encoder(8,'PC6','PC7') initiates timer 8 on
    pins PC6 and PC7'''
   
    def __init__ (self, timer_num, pin_1, pin_2):
        
        ''' Initializes the encoders used on the motors
        by initializing pins, and setting initial position to '0'
        
        @param pin_1 A pyb.Pin object corresponding to the pin used for encoder channel A.
        @param pin_2 A pyb.Pin object corresponding to the pin used for encoder channel B.
        @param timer_num A pyb.Timer object to use for tracking the encoder position '''
        print ('Initializing motor encoder')
        
        
        self.timer = pyb.Timer(timer_num,prescaler=0,period=65535)
        
        self.pin_Ch_1 = pyb.Pin(pin_1, pyb.Pin.IN)
        self.pin_Ch_2 = pyb.Pin(pin_2, pyb.Pin.IN)
        
        self.Ch_1 = self.timer.channel(1, pyb.Timer.ENC_A, pin=self.pin_Ch_1)
        self.Ch_2 = self.timer.channel(2, pyb.Timer.ENC_B, pin=self.pin_Ch_2)

        self.curr_position = 0
        self.prev_position = 0
        self.delta = 0
        self.position = 0
        self.set_position_val = 0
        
    def update(self):
        '''This method reads from the counter, and stores it'''
        self.curr_position = self.timer.counter()
        
        self.delta = self.curr_position - self.prev_position
        
        # In case we have overflow or underflow, we will need to check the 
        # following...
        #
        # In the scenario of an underflow,
        if (self.delta > 65535/2):
            self.delta -= 65535
            
            self.position += self.delta
            
        # In the scenario of an overflow,
        elif (self.delta < ((-65535/2)+1)):
            self.delta += 65535
            self.position += self.delta
            
        # If there was no underflow or overflow,
        else:
            self.position += self.delta
        self.prev_position = self.curr_position
        
    def get_position(self):
        '''This method returns the total number of encoder ticks'''
        return self.position
        
    def set_position(self, set_position_val):
        '''This method allows the user to define a specific position value'''
        self.set_position_val = set_position_val
        self.position = self.set_position_val    
        
    def get_delta(self):
        '''This method returns the delta from the two most recent positions'''
        return self.delta
    