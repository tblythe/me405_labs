## @file mainpage.py
#  @author Trevor Blythe
#  @mainpage
#
#  @section sec_intro Introduction
#  Lab 1 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_1/  
#
#  @section sec_motor Motor Driver
#  Motor driver lab that runs motor based on duty cycle.
#
#  @section sec_enc Encoder Lab 2
#  Lab 2 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_2/
#
#  @section sec_lin Lab 3
#  Lab 2 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_3/
#
#  @section sec_imc Lab 4
#  Lab 2 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_4/
