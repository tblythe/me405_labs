'''
@file controller.py
'''

class controller:
    '''
    This class implements closed-loop proprtional control of a motor. 
    @param kp Proportional gain of the motor  
    @param setpoint Desired location of the motor
    @param error_signal
    @param acuation_signal
    
    '''
    def __init__ (self, kp, setpoint):
        '''
        Constructor method for intializing parameters necessary
        for proportional control.        
        '''
        self.kp = kp
        
        self.setpoint = setpoint
        
        #Temporary test of actualpoint
        self.actualpoint = 0
        self.error_signal = 0
    def set_kp(self, gain):
        self.kp = kp
    #    def update (self, actualpoint, setpoint, kp, error_signal):

    def update (self, actualpoint, setpoint):
        self.actualpoint = actualpoint
        self.error_signal = self.setpoint-self.actualpoint
        self.actuation_signal = self.error_signal*self.kp
        return self.actuation_signal
   
    