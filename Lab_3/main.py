'''
@file main.py
'''

import utime
import pyb
import array

from controller import controller
from encoder import encoder
from MotorDriver import motordriver

controller = controller(0.5, 1000)

pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)       
pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4)
pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5)
encoder = encoder(4,'PB6','PB7')
# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq=20000)    

motor = motordriver(pin_EN, pin_IN1, pin_IN2, tim)
motor.set_duty(10)
motor.enable()
delta_total = 0
xcc = 0
position_array = array.array('i', [])
time_array = array.array('i', [])


for x in range(300):
    encoder.update()
    encoder.get_position()
    encoder.get_delta()
    
    delta_total = delta_total + encoder.delta
    #absolute value of delta_total here
    delta_abs = abs(delta_total)
    
    actuation_signal = controller.update(delta_abs,controller.setpoint)
    motor.set_duty(actuation_signal)
    
    #if (abs(actuation_signal) < 10):
    #    motor.set_duty(10)
    #else:
    #    motor.set_duty(actuation_signal)
    position_array.append(encoder.curr_position)
    time_array.append(utime.ticks_ms())
    utime.sleep_ms(10)
    
else:
    motor.disable

for j in range(300):
    print(time_array[j],',',position_array[j])
    
    
    


