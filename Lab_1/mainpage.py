## @file mainpage.py
#  @author Trevor Blythe
#  @mainpage
#
#  @section sec_intro Introduction
#  Lab 1 for ME 405 Spring 2020
#  The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_1/  
#
#  @section Motor Driver
#  Motor driver lab that runs motor based on duty cycle.
#
# @section
# Lab 2 for ME 405 Spring 2020
# The code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_2/