## @file MotorDriver.py
#  Motor driver for ME 405 
#  Code is at https://bitbucket.org/tblythe/me405_labs/src/master/Lab_1/
#  @author Trevor Blythe
#
#  @date 4/25/2020
#
#  @package MotorDriver
#  Motor Driver for ME 405
#

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''
    ## Constructor for MotorDriver
    #
    #      
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
            and IN2_pin. '''
        print ('Creating a motor driver')        
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
    ## Starts the motor by setting EN_pin to high
    #
    #          
    def enable (self):
        print ('Enabling Motor')
        self.EN_pin.high()
    ## Stops the motor by setting EN_pin to low
    #
    #  
    def disable (self):
        print ('Disabling Motor')
        self.EN_pin.low()
    ## Sets the duty cycle for the motor. Positive values makes the motor
    #    turn one direction, while negative values make it turn in the opposite direction.
    #    @param duty A signed integer representing the duty cycle
    #
    #  
    def set_duty (self, duty):
        t3ch1 = self.timer.channel(1, pyb.Timer.PWM, pin=pin_IN1)
        t3ch2 = self.timer.channel(2, pyb.Timer.PWM, pin=pin_IN2)
        if duty >= 0:
            self.IN1_pin.low()
            t3ch1.pulse_width_percent(duty)
        else:
            self.IN2_pin.low()
            t3ch2.pulse_width_percent(-duty) 

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    import pyb
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)       
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5)
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000)
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

    # Disable the motor driver to start with
    moe.disable()
    
    # Set the duty cycle to 10 percent
    moe.set_duty(10)
    
    
